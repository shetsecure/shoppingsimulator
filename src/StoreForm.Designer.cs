﻿namespace CashSys {
    partial class StoreForm {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(StoreForm));
            this.productsPanel = new System.Windows.Forms.FlowLayoutPanel();
            this.boughtPanel = new System.Windows.Forms.FlowLayoutPanel();
            this.scannerPanel = new System.Windows.Forms.Panel();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.btn_pay = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.lbl_total_txt = new System.Windows.Forms.Label();
            this.lbl_total = new System.Windows.Forms.Label();
            this.lbl_MAD = new System.Windows.Forms.Label();
            this.lbl_solde_txt = new System.Windows.Forms.Label();
            this.lbl_solde_value = new System.Windows.Forms.Label();
            this.scannerPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // productsPanel
            // 
            this.productsPanel.AllowDrop = true;
            this.productsPanel.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.productsPanel.Location = new System.Drawing.Point(524, 265);
            this.productsPanel.Name = "productsPanel";
            this.productsPanel.Size = new System.Drawing.Size(705, 422);
            this.productsPanel.TabIndex = 0;
            this.productsPanel.TabStop = true;
            // 
            // boughtPanel
            // 
            this.boughtPanel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.boughtPanel.Location = new System.Drawing.Point(23, 231);
            this.boughtPanel.Name = "boughtPanel";
            this.boughtPanel.Size = new System.Drawing.Size(438, 405);
            this.boughtPanel.TabIndex = 1;
            // 
            // scannerPanel
            // 
            this.scannerPanel.AllowDrop = true;
            this.scannerPanel.Controls.Add(this.pictureBox1);
            this.scannerPanel.Location = new System.Drawing.Point(524, 12);
            this.scannerPanel.Name = "scannerPanel";
            this.scannerPanel.Size = new System.Drawing.Size(291, 181);
            this.scannerPanel.TabIndex = 2;
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackgroundImage = global::CashSys.Properties.Resources.SCBTH_front_hand;
            this.pictureBox1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox1.Location = new System.Drawing.Point(0, 0);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(294, 183);
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            // 
            // btn_pay
            // 
            this.btn_pay.Location = new System.Drawing.Point(23, 642);
            this.btn_pay.Name = "btn_pay";
            this.btn_pay.Size = new System.Drawing.Size(75, 23);
            this.btn_pay.TabIndex = 3;
            this.btn_pay.Text = "Pay";
            this.btn_pay.UseVisualStyleBackColor = true;
            this.btn_pay.Click += new System.EventHandler(this.btn_pay_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(861, 231);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(95, 13);
            this.label1.TabIndex = 4;
            this.label1.Text = "Available Products";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(195, 196);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(93, 13);
            this.label2.TabIndex = 5;
            this.label2.Text = "Selected products";
            // 
            // lbl_total_txt
            // 
            this.lbl_total_txt.AutoSize = true;
            this.lbl_total_txt.Location = new System.Drawing.Point(335, 647);
            this.lbl_total_txt.Name = "lbl_total_txt";
            this.lbl_total_txt.Size = new System.Drawing.Size(37, 13);
            this.lbl_total_txt.TabIndex = 6;
            this.lbl_total_txt.Text = "Total: ";
            // 
            // lbl_total
            // 
            this.lbl_total.AutoSize = true;
            this.lbl_total.Location = new System.Drawing.Point(378, 647);
            this.lbl_total.Name = "lbl_total";
            this.lbl_total.Size = new System.Drawing.Size(28, 13);
            this.lbl_total.TabIndex = 7;
            this.lbl_total.Text = "0.00";
            // 
            // lbl_MAD
            // 
            this.lbl_MAD.AutoSize = true;
            this.lbl_MAD.Location = new System.Drawing.Point(426, 647);
            this.lbl_MAD.Name = "lbl_MAD";
            this.lbl_MAD.Size = new System.Drawing.Size(31, 13);
            this.lbl_MAD.TabIndex = 8;
            this.lbl_MAD.Text = "MAD";
            // 
            // lbl_solde_txt
            // 
            this.lbl_solde_txt.AutoSize = true;
            this.lbl_solde_txt.Location = new System.Drawing.Point(1074, 12);
            this.lbl_solde_txt.Name = "lbl_solde_txt";
            this.lbl_solde_txt.Size = new System.Drawing.Size(66, 13);
            this.lbl_solde_txt.TabIndex = 9;
            this.lbl_solde_txt.Text = "Votre solde: ";
            // 
            // lbl_solde_value
            // 
            this.lbl_solde_value.AutoSize = true;
            this.lbl_solde_value.Location = new System.Drawing.Point(1146, 12);
            this.lbl_solde_value.Name = "lbl_solde_value";
            this.lbl_solde_value.Size = new System.Drawing.Size(0, 13);
            this.lbl_solde_value.TabIndex = 10;
            // 
            // StoreForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1241, 739);
            this.Controls.Add(this.lbl_solde_value);
            this.Controls.Add(this.lbl_solde_txt);
            this.Controls.Add(this.lbl_MAD);
            this.Controls.Add(this.lbl_total);
            this.Controls.Add(this.lbl_total_txt);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btn_pay);
            this.Controls.Add(this.productsPanel);
            this.Controls.Add(this.boughtPanel);
            this.Controls.Add(this.scannerPanel);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "StoreForm";
            this.Text = "CustomerForm";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.scannerPanel.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.FlowLayoutPanel productsPanel;
        private System.Windows.Forms.FlowLayoutPanel boughtPanel;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Panel scannerPanel;
        private System.Windows.Forms.Button btn_pay;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label lbl_total_txt;
        private System.Windows.Forms.Label lbl_total;
        private System.Windows.Forms.Label lbl_MAD;
        private System.Windows.Forms.Label lbl_solde_txt;
        private System.Windows.Forms.Label lbl_solde_value;
    }
}