﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CashSys {
    public partial class AuthForm : Form {
        public AuthForm() {
            InitializeComponent();
            Program.auth = this;
        }

        private void btn_connect_Click(object sender, EventArgs e) {
            var connection = AuthDB.connectDB();

            if (connection != null) {

                using (connection)
                using (var command = connection.CreateCommand()) {
                    command.CommandText = "SELECT count(1) FROM User WHERE username = @username AND password = @password";
                    command.Parameters.AddWithValue("@username", txt_username.Text);
                    command.Parameters.AddWithValue("@password", txt_password.Text);
                    int r = 0;
                    using (var reader = command.ExecuteReader())
                        while (reader.Read()) {
                            r = Convert.ToInt32(reader.GetString(0));
                            break;
                        }

                    if(r > 0) {
                        AdminChoiceForm choiceForm;
                        var initForm = new InitMachineForm();
                        if (InitMachineForm.count == 1)
                            Program.initForm.Show();
                        else {
                            choiceForm = new AdminChoiceForm();
                            choiceForm.Show();
                        }
                        Hide();
                    }
                    else {
                        MessageBox.Show("Wrong credentials !", "Failed", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }

            }
            else {
                MessageBox.Show("Cannot connect to database. !", "Failed", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        ~AuthForm() {
            //Application.ExitThread();
            //Application.Exit();
            //Close();
        }
    }
}
