﻿namespace CashSys {
    partial class InitMachineForm {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(InitMachineForm));
            this.btn_init = new System.Windows.Forms.Button();
            this.lbl_title = new System.Windows.Forms.Label();
            this.pictureBox5 = new System.Windows.Forms.PictureBox();
            this.pictureBox10 = new System.Windows.Forms.PictureBox();
            this.pictureBox20 = new System.Windows.Forms.PictureBox();
            this.pictureBox50 = new System.Windows.Forms.PictureBox();
            this.pictureBox100 = new System.Windows.Forms.PictureBox();
            this.pictureBox200 = new System.Windows.Forms.PictureBox();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.txt1 = new System.Windows.Forms.TextBox();
            this.txt2 = new System.Windows.Forms.TextBox();
            this.txt5 = new System.Windows.Forms.TextBox();
            this.txt10 = new System.Windows.Forms.TextBox();
            this.txt20 = new System.Windows.Forms.TextBox();
            this.txt50 = new System.Windows.Forms.TextBox();
            this.txt100 = new System.Windows.Forms.TextBox();
            this.txt200 = new System.Windows.Forms.TextBox();
            this.lbl_error = new System.Windows.Forms.Label();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox20)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox50)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox100)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox200)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            this.SuspendLayout();
            // 
            // btn_init
            // 
            this.btn_init.Location = new System.Drawing.Point(443, 402);
            this.btn_init.Name = "btn_init";
            this.btn_init.Size = new System.Drawing.Size(115, 23);
            this.btn_init.TabIndex = 35;
            this.btn_init.Text = "Initialiser";
            this.btn_init.UseVisualStyleBackColor = true;
            this.btn_init.Click += new System.EventHandler(this.btn_init_Click);
            // 
            // lbl_title
            // 
            this.lbl_title.AutoSize = true;
            this.lbl_title.Font = new System.Drawing.Font("Microsoft Sans Serif", 18.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_title.Location = new System.Drawing.Point(418, 72);
            this.lbl_title.Name = "lbl_title";
            this.lbl_title.Size = new System.Drawing.Size(233, 29);
            this.lbl_title.TabIndex = 34;
            this.lbl_title.Text = "Initialiser La Caisse";
            // 
            // pictureBox5
            // 
            this.pictureBox5.BackgroundImage = global::CashSys.Properties.Resources._5;
            this.pictureBox5.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox5.Location = new System.Drawing.Point(537, 145);
            this.pictureBox5.Name = "pictureBox5";
            this.pictureBox5.Size = new System.Drawing.Size(100, 50);
            this.pictureBox5.TabIndex = 25;
            this.pictureBox5.TabStop = false;
            // 
            // pictureBox10
            // 
            this.pictureBox10.BackgroundImage = global::CashSys.Properties.Resources._10;
            this.pictureBox10.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox10.Location = new System.Drawing.Point(672, 145);
            this.pictureBox10.Name = "pictureBox10";
            this.pictureBox10.Size = new System.Drawing.Size(100, 50);
            this.pictureBox10.TabIndex = 24;
            this.pictureBox10.TabStop = false;
            // 
            // pictureBox20
            // 
            this.pictureBox20.BackgroundImage = global::CashSys.Properties.Resources._20;
            this.pictureBox20.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox20.Location = new System.Drawing.Point(275, 260);
            this.pictureBox20.Name = "pictureBox20";
            this.pictureBox20.Size = new System.Drawing.Size(100, 50);
            this.pictureBox20.TabIndex = 23;
            this.pictureBox20.TabStop = false;
            // 
            // pictureBox50
            // 
            this.pictureBox50.BackgroundImage = global::CashSys.Properties.Resources._50;
            this.pictureBox50.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox50.Location = new System.Drawing.Point(406, 260);
            this.pictureBox50.Name = "pictureBox50";
            this.pictureBox50.Size = new System.Drawing.Size(100, 50);
            this.pictureBox50.TabIndex = 22;
            this.pictureBox50.TabStop = false;
            // 
            // pictureBox100
            // 
            this.pictureBox100.BackgroundImage = global::CashSys.Properties.Resources._100;
            this.pictureBox100.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox100.Location = new System.Drawing.Point(537, 260);
            this.pictureBox100.Name = "pictureBox100";
            this.pictureBox100.Size = new System.Drawing.Size(100, 50);
            this.pictureBox100.TabIndex = 21;
            this.pictureBox100.TabStop = false;
            // 
            // pictureBox200
            // 
            this.pictureBox200.BackgroundImage = global::CashSys.Properties.Resources._200;
            this.pictureBox200.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox200.Location = new System.Drawing.Point(672, 260);
            this.pictureBox200.Name = "pictureBox200";
            this.pictureBox200.Size = new System.Drawing.Size(100, 50);
            this.pictureBox200.TabIndex = 20;
            this.pictureBox200.TabStop = false;
            // 
            // pictureBox2
            // 
            this.pictureBox2.BackgroundImage = global::CashSys.Properties.Resources._2;
            this.pictureBox2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox2.Location = new System.Drawing.Point(406, 145);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(100, 50);
            this.pictureBox2.TabIndex = 19;
            this.pictureBox2.TabStop = false;
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackgroundImage = global::CashSys.Properties.Resources._1;
            this.pictureBox1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox1.Location = new System.Drawing.Point(275, 145);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(100, 50);
            this.pictureBox1.TabIndex = 18;
            this.pictureBox1.TabStop = false;
            // 
            // txt1
            // 
            this.txt1.Location = new System.Drawing.Point(288, 201);
            this.txt1.Name = "txt1";
            this.txt1.Size = new System.Drawing.Size(70, 20);
            this.txt1.TabIndex = 36;
            // 
            // txt2
            // 
            this.txt2.Location = new System.Drawing.Point(423, 201);
            this.txt2.Name = "txt2";
            this.txt2.Size = new System.Drawing.Size(70, 20);
            this.txt2.TabIndex = 37;
            // 
            // txt5
            // 
            this.txt5.Location = new System.Drawing.Point(551, 201);
            this.txt5.Name = "txt5";
            this.txt5.Size = new System.Drawing.Size(70, 20);
            this.txt5.TabIndex = 38;
            // 
            // txt10
            // 
            this.txt10.Location = new System.Drawing.Point(687, 201);
            this.txt10.Name = "txt10";
            this.txt10.Size = new System.Drawing.Size(70, 20);
            this.txt10.TabIndex = 39;
            // 
            // txt20
            // 
            this.txt20.Location = new System.Drawing.Point(288, 316);
            this.txt20.Name = "txt20";
            this.txt20.Size = new System.Drawing.Size(70, 20);
            this.txt20.TabIndex = 40;
            // 
            // txt50
            // 
            this.txt50.Location = new System.Drawing.Point(423, 316);
            this.txt50.Name = "txt50";
            this.txt50.Size = new System.Drawing.Size(70, 20);
            this.txt50.TabIndex = 41;
            // 
            // txt100
            // 
            this.txt100.Location = new System.Drawing.Point(551, 316);
            this.txt100.Name = "txt100";
            this.txt100.Size = new System.Drawing.Size(70, 20);
            this.txt100.TabIndex = 42;
            // 
            // txt200
            // 
            this.txt200.Location = new System.Drawing.Point(687, 316);
            this.txt200.Name = "txt200";
            this.txt200.Size = new System.Drawing.Size(70, 20);
            this.txt200.TabIndex = 43;
            // 
            // lbl_error
            // 
            this.lbl_error.AutoSize = true;
            this.lbl_error.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_error.ForeColor = System.Drawing.Color.Red;
            this.lbl_error.Location = new System.Drawing.Point(667, 419);
            this.lbl_error.Name = "lbl_error";
            this.lbl_error.Size = new System.Drawing.Size(0, 13);
            this.lbl_error.TabIndex = 44;
            // 
            // pictureBox3
            // 
            this.pictureBox3.BackgroundImage = global::CashSys.Properties.Resources.touch_pad_cash_counter_cashier_coffee_shop_kiosk_sasflexpos_1511_11_sasflexpos_8;
            this.pictureBox3.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox3.Location = new System.Drawing.Point(33, 144);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(202, 166);
            this.pictureBox3.TabIndex = 45;
            this.pictureBox3.TabStop = false;
            // 
            // InitMachineForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(979, 483);
            this.Controls.Add(this.pictureBox3);
            this.Controls.Add(this.lbl_error);
            this.Controls.Add(this.txt200);
            this.Controls.Add(this.txt100);
            this.Controls.Add(this.txt50);
            this.Controls.Add(this.txt20);
            this.Controls.Add(this.txt10);
            this.Controls.Add(this.txt5);
            this.Controls.Add(this.txt2);
            this.Controls.Add(this.txt1);
            this.Controls.Add(this.btn_init);
            this.Controls.Add(this.lbl_title);
            this.Controls.Add(this.pictureBox5);
            this.Controls.Add(this.pictureBox10);
            this.Controls.Add(this.pictureBox20);
            this.Controls.Add(this.pictureBox50);
            this.Controls.Add(this.pictureBox100);
            this.Controls.Add(this.pictureBox200);
            this.Controls.Add(this.pictureBox2);
            this.Controls.Add(this.pictureBox1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "InitMachineForm";
            this.Text = "InitMachineForm";
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox20)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox50)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox100)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox200)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btn_init;
        private System.Windows.Forms.Label lbl_title;
        private System.Windows.Forms.PictureBox pictureBox5;
        private System.Windows.Forms.PictureBox pictureBox10;
        private System.Windows.Forms.PictureBox pictureBox20;
        private System.Windows.Forms.PictureBox pictureBox50;
        private System.Windows.Forms.PictureBox pictureBox100;
        private System.Windows.Forms.PictureBox pictureBox200;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.TextBox txt1;
        private System.Windows.Forms.TextBox txt2;
        private System.Windows.Forms.TextBox txt5;
        private System.Windows.Forms.TextBox txt10;
        private System.Windows.Forms.TextBox txt20;
        private System.Windows.Forms.TextBox txt50;
        private System.Windows.Forms.TextBox txt100;
        private System.Windows.Forms.TextBox txt200;
        private System.Windows.Forms.Label lbl_error;
        private System.Windows.Forms.PictureBox pictureBox3;
    }
}