﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace CashSys {
    abstract class Person {
        public string Name { get; set; }
        protected Tickets t = new Tickets(0);

        protected void initTickets(string callerName) {
            char r;
            Console.WriteLine($"Setting amount of the {callerName}");
            Console.WriteLine("Would you like to give the total amount and optimize it ? (y/n)");
            r = Console.ReadKey().KeyChar;
            if (r == 'y' || r == 'Y') {
                ulong total = Convert.ToUInt64(Console.ReadLine());
                t.optimize(total);
            }
            else {
                Dictionary<ushort, ushort> d = new Dictionary<ushort, ushort>();

                Console.Write("\nHow many ticket of 1 ? ");
                d.Add(1, Convert.ToUInt16(Console.ReadLine()));
                Console.Write("How many ticket of 2 ? ");
                d.Add(2, Convert.ToUInt16(Console.ReadLine()));
                Console.Write("How many ticket of 5 ? ");
                d.Add(5, Convert.ToUInt16(Console.ReadLine()));
                Console.Write("How many ticket of 10 ? ");
                d.Add(10, Convert.ToUInt16(Console.ReadLine()));
                Console.Write("How many ticket of 20 ? ");
                d.Add(20, Convert.ToUInt16(Console.ReadLine()));
                Console.Write("How many ticket of 50 ? ");
                d.Add(50, Convert.ToUInt16(Console.ReadLine()));
                Console.Write("How many ticket of 100 ? ");
                d.Add(100, Convert.ToUInt16(Console.ReadLine()));
                Console.Write("How many ticket of 200 ? ");
                d.Add(200, Convert.ToUInt16(Console.ReadLine()));

                t.setTickets(d);
            }
            Dictionary<ushort, ushort> _t = t.getTickets();

            Console.WriteLine("Displaying tickets...");
            ulong sum = 0;
            foreach (var ticket in _t.Keys.ToList()) {
                if(_t[ticket] != 0)
                    Console.WriteLine($"This person has {_t[ticket]} of {ticket}");

                sum += (ulong)_t[ticket] * ticket;
            }

            Console.WriteLine($"Total = {sum}");
        }

        public Tickets getTickets() {
            return t;
        }
    }
}
