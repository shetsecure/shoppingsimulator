﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace CashSys {
    public class Product {
        private string token = "secretToken";
        private uint percentage; // for promotion
        private DateTime start, end; // for promotion

        public Product() {

        }

        public Product(string name, double price, ulong cab, Image img) {
            setName(name, token);
            setPrice(price, token);
            setCab(cab, token);
            Image = img;
        }

        public Image Image { get; private set; }
        public bool setImage(string path, string token) {
            if(token == this.token) {
                Image = Image.FromFile(path);
                Log.addInfo($"new Product created with this image: {Image.ToString()}");
                return true;
            }
            return false;
        }

        public string Name { get; private set; }
        public bool setName(string name, string token) {
            if (token == this.token) {  
                Name = name;
                Log.addInfo($"new Product created under this name : {Name}");
                return true;
            }
            else {
                Console.WriteLine("Token is wrong!");
                return false;
            }
        }

        public double Price { get; private set; }
        public bool setPrice(double price, string token) {
            if(token == this.token) {
                if (price > 0.0f) {
                    Price = price;
                    Log.addInfo($"Product {Name} have this price: {Price}");
                    return true;
                }
                else {
                    Console.WriteLine("Price must be positive.");
                    return false;
                }
            }
            else {
                Console.WriteLine("Token is wrong!");
                return false;
            }
        }

        public ulong Cab { get; private set; }
        public bool setCab(ulong cab, string token) {
            if(this.token == token) {
                if(cab > 0) {
                    Cab = cab;
                    Log.addInfo($"{Name} is identified by {Cab}");
                    return true;
                }
                else {
                    Console.WriteLine("Cab must be positive.");
                    return false;
                }
            }
            else {
                Console.WriteLine("Wrong token.");
                return false;
            }
        }

        public bool isPromoted { get; private set; } = false;
        public bool setPromotion(uint percentage, DateTime start, DateTime end, string token) {
            if(token == this.token) {
                isPromoted = true;
                this.percentage = percentage;
                this.start = start;
                this.end = end;
                Log.addInfo($"{Name} is in promotion from {start} to {end} with {percentage}");
                return true;
            }
            else {
                Console.WriteLine("Wrong token.");
                return false;
            }
        }

    }
}
