﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CashSys {
    static class Log {
        static string path = @"C:\Users\shetsecure\source\repos\ConsoleApp3\logs.txt";
        static TextWriter tw;
        static DateTime now = DateTime.UtcNow;

        static Log() {
            openFile();
            
        }

        static void openFile() {
            if (!File.Exists(path)) {
                File.Create(path);
                tw = new StreamWriter(path);
            }
        }
        
        public static void addInfo(string info, [System.Runtime.CompilerServices.CallerFilePath] string sourceFilePath = "Unknown") {
            int index = sourceFilePath.LastIndexOf('\\') + 1;
            int len = sourceFilePath.LastIndexOf('.') - index;
            string callerClassName = sourceFilePath.Substring(index, len);

            if (File.Exists(path)) {
                using (var tw = new StreamWriter(path, true)) {
                    tw.WriteLine("[INFO] " + now.ToString() + " " + callerClassName + ": " + info + ".");
                    tw.Close();
                }
            }
            else
                Console.WriteLine("Cannot open file.");
        }

        public static void addWarning(string warn, [System.Runtime.CompilerServices.CallerFilePath] string sourceFilePath = "Unknown") {
            int index = sourceFilePath.LastIndexOf('\\') + 1;
            int len = sourceFilePath.LastIndexOf('.') - index;
            string callerClassName = sourceFilePath.Substring(index, len);

            if (File.Exists(path)) {
                using (var tw = new StreamWriter(path, true)) {
                    tw.WriteLine("[Warning] " + now.ToString() + " " + callerClassName + ": " + warn + ".");
                    tw.Close();
                }
            }
            else
                Console.WriteLine("Cannot open file.");
                       
        }

        public static void addError(string error, [System.Runtime.CompilerServices.CallerFilePath] string sourceFilePath = "Unknown") {
            int index = sourceFilePath.LastIndexOf('\\') + 1;
            int len = sourceFilePath.LastIndexOf('.') - index;
            string callerClassName = sourceFilePath.Substring(index, len);

            if (File.Exists(path)) {
                using (var tw = new StreamWriter(path, true)) {
                    tw.WriteLine("[Error] " + now.ToString() + " " + callerClassName + ": " + error + ".");
                    tw.Close();
                }
            }
            else
                Console.WriteLine("Cannot open file.");

        }
    }
}
