﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;

namespace CashSys {
    class Customer : Person {
        //private string login, password;
        //private Cart cart;

        public Customer() {
           // initTickets("Customer");
            //cart = new Cart();
            Log.addInfo("New Customer is here");
        }

        public bool pay(Machine m, ulong amount_to_pay) {
            if (t.getTotalAmount() < amount_to_pay) { // add to Log
                Console.WriteLine("Cannot proceed. Needs more tickets.");
                //MessageBox.Show("Cannot proceed. Needs more tickets.");
                return false;
            }
            else {
                Dictionary<ushort, ushort> best;
                if (t.pay(amount_to_pay, out best)) { // will give us the tickets that adds up to the min total > amount_to_pay + decreasing those from solde
                    Tickets returned_tickets_from_machine = m.giveChange(best, amount_to_pay); // will add best to machine + return the change
                    t.addTickets(returned_tickets_from_machine.getTickets()); // add tickets to this customer packet
                    return true;
                }
                MessageBox.Show("failed");
                return false;
            }
        }

        // deprecated, DO NOT USE
        public void pay(Machine m, Tickets t_entered, ulong amount_to_pay) { 
            if(t_entered.getTotalAmount() < amount_to_pay) {
                Console.WriteLine("Cannot proceed. Needs more tickets.");
                return ;
            }
            else {

                t.decreaseTickets(t_entered); // payed, decrease it
                //m.getTickets().addTickets(t_entered.getTickets()); // add tickets to the machine
                Tickets returned_tickets_from_machine = m.giveChange(t_entered.getTickets(), amount_to_pay);

                t.addTickets(returned_tickets_from_machine.getTickets()); // add tickets to this customer packet
                // always add it, cuz it will be either the change or his input 
                //(if the machine returns the input due 
                // to non sufficient tickets to express 
            }
        }

    }
}
