﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CashSys {
    public partial class AdminChoiceForm : Form {

        public AdminChoiceForm() {
            InitializeComponent();
            Program.adminChoiceForm = this;
        }

        private void btn_caisse_Click(object sender, EventArgs e) {
            try {
                Program.initForm.Activate();
                Program.initForm.ShowDialog();
            } catch(Exception ex) {
                Program.initForm.Show();
            }
        }

        private void btn_produit_Click(object sender, EventArgs e) {
            var pManagementForm = new AdminForm();
            pManagementForm.Show();
            Program.pManagement = pManagementForm;
        }

        ~AdminChoiceForm() {
            Application.ExitThread();
            Application.Exit();
            Close();
        }
    }
}
