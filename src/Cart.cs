﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CashSys {
    class Cart {
        uint id;
        static uint count = 0;
        List<Product> cart;
        
        public Cart() {
            cart = new List<Product>();
            id = ++count;
            Log.addInfo($"New Cart Created identified by {id}");
        }

        public void addProduct(Product p ) {
            cart.Add(p);
            Log.addInfo($"New Product added to cart number {id}");
        }

        //public bool removeProduct(Product p) {
        //    if (cart.Remove(cart.Find(p)))
        //        return true;
        //    return false;
        //}

        public List<Product> getAllProducts() {
            return cart;
        }

        public int getNumberOfProducts() {
            return cart.Count();
        }

        public void empty() {
            cart.Clear();
            Log.addInfo($"Cart number {id} is emptied");
        }
        
    }
}
