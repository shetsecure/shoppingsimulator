﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CashSys {
    static class AuthDB {
        private static Dictionary<String, String> credentials = new Dictionary<string, string>();
        private static string token = "secretToken";

        
        public static bool login(string username, string password) {
            if(credentials.ContainsKey(username) && credentials[username] == password) 
                return true;

            Console.WriteLine("Combination of username/password is wrong.");
            return false;
        }

        public static bool updatePassword(string username, string oldPassword) {
            if (login(username, oldPassword)) {
                Console.WriteLine("Enter the new password");
                string newPassword = Console.ReadLine();
                while (newPassword.Length < 6) {
                    Console.WriteLine("Please provide a password that have at least 6 chars.");
                    newPassword = Console.ReadLine();
                }

                credentials[username] = newPassword;
                return true;
            }
            else {
                Console.WriteLine("Credentials don\'t match !");
                return false;
            }
        }

        // admin privileges
        public static string getPassword(string username, string token) {
            if (token == AuthDB.token)
                return credentials[username];
            else {
                Console.WriteLine("Wrong token.");
                return "";
            }
        }

        public static bool addUser(string username, string password, string token) {
            if(token == AuthDB.token) {
                if(credentials.ContainsKey(username)) {
                    Console.WriteLine("This username already exists !");
                    return false;
                }

                credentials[username] = password;
                return true;
            }

            else {
                Console.WriteLine("Wrong token.");
                return false;
            }
        }

        public static bool editUserPassword(string username, string newPassword, string token) {
            if (token == AuthDB.token) {
                if (credentials.ContainsKey(username)) {
                    credentials[username] = newPassword;
                    Console.WriteLine("Password updated successfully.");
                    return true;
                }
                else {
                    Console.WriteLine($"{username} doesn\'t exist !");

                    return false;
                }
            }

            else {
                Console.WriteLine("Wrong token.");
                return false;
            }
        }

        public static bool deleteUser(string username, string token) {
            if(token == AuthDB.token) {
                if (credentials.Remove(username)) {
                    Console.WriteLine($"{username} is successfully removed.");
                    return true;
                }
                else {
                    Console.WriteLine("This username doesn\'t exist !");
                    return true;
                }
            }
            else {
                Console.WriteLine("bad token !");
                return false;
            }
        }

        public static MySqlConnection connectDB() {
            //MySqlConnection MySQLConnection = new MySqlConnection();
            //MySqlDataAdapter MySQLAdapter = new MySqlDataAdapter();
            string connString = "Server=localhost;Database=CashSys;uid=root;pwd=;";
            MySqlConnection conn = new MySqlConnection(connString);

            //string location = "C:\\Users\\shetsecure\\source\\repos\\CashSystem - Copy\\CashSystem\\CashSystem.mdf";
            //SqlConnection conn = new SqlConnection("Data Source=(LocalDB)\\MSSQLLocalDB;AttachDbFilename=" + location +
            //";Integrated Security=True;Connect Timeout=30");
            //SqlConnection conn = new SqlConnection('Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename="C:\\Users\\shetsecure\\source\\repos\\CashSystem - Copy\\CashSystem\\CashSystem.mdf";Integrated Security=True;Connect Timeout=30');

            try {
                conn.Open();
                return conn;
            } catch (Exception e) {
                Console.WriteLine(e.Message);
                return null;
            }
        }
    }
}
