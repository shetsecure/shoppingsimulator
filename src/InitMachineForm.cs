﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CashSys {
    public partial class InitMachineForm : Form {
        private static TextBox[] txt_arr = new TextBox[8];
        private static Dictionary<ushort, ushort> bills = Program.m.getTickets().getTickets();
        private bool all_good = true;
        public static int count = 1;

        public InitMachineForm() {
            InitializeComponent();
            Program.initForm = this;
            constructBillArray();
            
            if (count > 1) {
                lbl_title.Text = "Gérer La Caisse";
                btn_init.Text = "Modifier";
                bills = Program.m.getTickets().getTickets(); // updating the machine 
                updateTexts();
                //bills = Program.m.getTickets().getTickets().ToDictionary(entry => entry.Key, entry => entry.Value);
                //MessageBox.Show(Program.m.getTickets().getTotalAmount().ToString());
            }
        }

        public void updateTexts() {
            txt_arr[0].Text = bills[1].ToString();
            txt_arr[1].Text = bills[2].ToString();
            txt_arr[2].Text = bills[5].ToString();
            txt_arr[3].Text = bills[10].ToString();
            txt_arr[4].Text = bills[20].ToString();
            txt_arr[5].Text = bills[50].ToString();
            txt_arr[6].Text = bills[100].ToString();
            txt_arr[7].Text = bills[200].ToString();
            //for (int i = 0; i < 8; i++)
            //    txt_arr[i].Text = "0";
        }

        private void constructBillArray() {
            txt_arr[0] = txt1;
            txt_arr[1] = txt2;
            txt_arr[2] = txt5;
            txt_arr[3] = txt10;
            txt_arr[4] = txt20;
            txt_arr[5] = txt50;
            txt_arr[6] = txt100;
            txt_arr[7] = txt200;

            updateTexts();

            for (int i = 0; i < 8; i++) 
                txt_arr[i].TextChanged += new EventHandler(txt_bill_TextChanged);
        }

        private void txt_bill_TextChanged(object sender, EventArgs e) {
            for (int i = 0; i < 8; i++) {
                try {
                    if (Convert.ToUInt16(txt_arr[i].Text) < 0) {
                        all_good = false;
                        break;
                    }
                    else {
                        all_good = true;
                        lbl_error.Text = "";
                    }
                }
                catch (Exception ex) {
                    all_good = false;
                    break;
                }
            }

            if(all_good) 
                btn_init.Enabled = true;
            else {
                btn_init.Enabled = false;
                lbl_error.Text = "Please insert positive integers in the textBoxs";
            }
        }

        private void btn_init_Click(object sender, EventArgs e) {
            if(all_good) {
                bills[1] = Convert.ToUInt16(txt_arr[0].Text);
                bills[2] = Convert.ToUInt16(txt_arr[1].Text);
                bills[5] = Convert.ToUInt16(txt_arr[2].Text);
                bills[10] = Convert.ToUInt16(txt_arr[3].Text);
                bills[20] = Convert.ToUInt16(txt_arr[4].Text);
                bills[50] = Convert.ToUInt16(txt_arr[5].Text);
                bills[100] = Convert.ToUInt16(txt_arr[6].Text);
                bills[200] = Convert.ToUInt16(txt_arr[7].Text);
            }

            count++;
            Program.m.getTickets().setTickets(bills);
            updateTexts();
            //Visible = false;
            Hide();

            //var myform = new MyForm();
            //Program.myform = myform;
            Program.myform.Show();
        }

        ~InitMachineForm() {
            //Application.ExitThread();
            //Application.Exit();
            Close();
        }
    }
}
