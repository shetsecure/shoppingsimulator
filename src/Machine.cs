﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Drawing;

namespace CashSys {
    class Machine {
        private static uint count = 0;
        private uint id; // machine ID
        private bool used = false; // machine state. Connected or !Connected to a cashier
        private string usedBy;
        private Tickets tickets = new Tickets(0);

        public Image Image { get; private set; }

        public Machine() {
            id = count++;
            Log.addInfo($"New Machine added to the store under the ID of {id}");
        }

        public Machine(Image i) : this() {
            Image = i;
        }

        public bool setImage(string path, [System.Runtime.CompilerServices.CallerFilePath] string sourceFilePath = "") {
            int index = sourceFilePath.LastIndexOf('\\') + 1;
            int len = sourceFilePath.LastIndexOf('.') - index;
            string callerClassName = sourceFilePath.Substring(index, len);

            if (callerClassName == "Cashier") {
                Image = Image.FromFile(path);
                Log.addInfo($"new Product created with this image: {Image.ToString()}");
                return true;
            }
            return false;
        }

        public uint getId() {
            return id;
        }

        public bool isConnected() {
            if (used)
                return true;
            return false;
        }

        public void setConnection(string cashier, [System.Runtime.CompilerServices.CallerFilePath] string sourceFilePath = "") {
            int index = sourceFilePath.LastIndexOf('\\') + 1;
            int len = sourceFilePath.LastIndexOf('.') - index;
            string callerClassName = sourceFilePath.Substring(index, len);

            if (callerClassName == "Cashier") {
                used = true;
                usedBy = cashier;
            }
            else
                Console.WriteLine("You\'re not allowed to terminate the session.");

        }

        public void endConnection(string cashier, [System.Runtime.CompilerServices.CallerFilePath] string sourceFilePath = "") {
            int index = sourceFilePath.LastIndexOf('\\') + 1;
            int len = sourceFilePath.LastIndexOf('.') - index;
            string callerClassName = sourceFilePath.Substring(index, len);

            if (callerClassName == "Cashier") {
                used = false;
                usedBy = "";
                Console.WriteLine("Session terminated sucessfully.");
            }
            else 
                Console.WriteLine("You\'re not allowed to terminate the session.");
            
        }

        public Tickets giveChange(Dictionary<ushort, ushort> input, ulong how_much) {
            // input is what the customer gave to the machine
            // how_much = the amount in the bill

            tickets.addTickets(input);
            ulong total_input = (ulong) input.Sum(x => x.Value);
            
            if (how_much > total_input) {
                Console.WriteLine($"The Customer needs to add an amount of {how_much - total_input}");
                tickets.decreaseTickets(input);
                return new Tickets(input);
            }
            else {
                ulong change = total_input - how_much;
                
                if ( tickets.getTotalAmount() < change ) {
                        Console.WriteLine("Cannot gave change. Not enough money in the machine.");
                        tickets.decreaseTickets(input);
                        return new Tickets(input);
                }
                else {
                        Tickets change_tickets; // can't be null, will be edited by the method below.
                        tickets.express(change, out change_tickets);
                        tickets.decreaseTickets(change_tickets);
                        return change_tickets;
                }
            }
        }

        public Tickets getTickets() {
            return tickets;
        }
    }
}
