﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CashSys {
    class Program {
        public static Machine m = new Machine();
        public static InitMachineForm initForm;
        public static AdminForm pManagement;
        public static AdminChoiceForm adminChoiceForm;
        public static MyForm myform;
        public static AuthForm auth;
        public static StoreForm store;
        [STAThread]
        static void Main(string[] args) {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new MyForm());
            //Application.Run(new BillForm());
            //var connection = AuthDB.connectDB();
            //using (connection)
            //using (var command = connection.CreateCommand()) {
            //    //connection.Open();
            //    command.CommandText = "select cab from Produit";
            //    using (var reader = command.ExecuteReader())
            //        while (reader.Read())
            //            Console.WriteLine(reader.GetString(0));
            //}
        }

        ~Program() {
            try {
                initForm.Close();
                pManagement.Close();
                adminChoiceForm.Close();
                myform.Close();
                auth.Close();
                //auth = null;
                //myform = null;
                //adminChoiceForm = null;
                //pManagement = null;
                //initForm = null;
                //Application.ExitThread();
                //Application.Exit();
            } catch (Exception ex) {
                Log.addError(ex.Message);
            }
        }
    }
}

