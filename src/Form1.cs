﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CashSys {
    public partial class Form1 : Form {
        private Button[] pImages;
        private MySqlConnection conn = AuthDB.connectDB();
        private MySqlCommand command;
        private MySqlDataReader reader;
        private int len;
        private Machine m = new Machine();
        private Customer c = new Customer(); // need to identify the customer

        public Form1() {
            InitializeComponent();
            loadProducts();
            loadProducts();
            addScrollBar();
            enableDragDrop();
        }

        private void loadProducts() {
            if (conn != null) {
                command = new MySqlCommand("SELECT COUNT(1) FROM Produit", conn);
                reader = command.ExecuteReader();

                while (reader.Read())
                    len = Convert.ToInt32(reader[0].ToString());

                pImages = new Button[len];
                reader.Close();


                command = new MySqlCommand("SELECT image, name, price FROM Produit", conn);
                reader = command.ExecuteReader();

                while (reader.Read()) {
                    len--;
                    Label name = new Label();
                    Label price = new Label();
                    pImages[len] = new Button();

                    pImages[len].Width = 100;
                    pImages[len].Height = 100;
                    pImages[len].BackgroundImageLayout = ImageLayout.Stretch;

                    if (reader["image"].ToString() != "")
                        pImages[len].BackgroundImage = Image.FromFile(reader["image"].ToString());

                    name.Text = reader["name"].ToString();
                    price.Text = reader["price"].ToString() + " MAD";

                    productsPanel.Controls.Add(pImages[len]);
                    productsPanel.Controls.Add(name);
                    productsPanel.Controls.Add(price);
                }

                reader.Close();
            }
        }

        private void addScrollBar() {
            productsPanel.AutoScroll = false;
            productsPanel.HorizontalScroll.Enabled = false;
            productsPanel.HorizontalScroll.Visible = false;
            productsPanel.HorizontalScroll.Maximum = 0;
            productsPanel.AutoScroll = true;

            boughtPanel.AutoScroll = false;
            boughtPanel.HorizontalScroll.Enabled = false;
            boughtPanel.HorizontalScroll.Visible = false;
            boughtPanel.HorizontalScroll.Maximum = 0;
            boughtPanel.AutoScroll = true;
        }

        private void panel_DragEnter(object sender, DragEventArgs e) {
            e.Effect = DragDropEffects.Move;
        }

        private void panel_DragDrop(object sender, DragEventArgs e) {
            ((Button)e.Data.GetData(typeof(Button))).Parent = (Panel)sender;
        }

        private void enableDragDrop() {
            productsPanel.AllowDrop = true;
            boughtPanel.AllowDrop = true;

            productsPanel.DragEnter += panel_DragEnter;
            boughtPanel.DragEnter += panel_DragEnter;

            productsPanel.DragDrop += panel_DragDrop;
            boughtPanel.DragDrop += panel_DragDrop;

            int l = pImages.Length;

            for (int i = 0; i < l; i++)
                pImages[i].MouseDown += button_MouseDown;
    }

    void button_MouseDown(object sender, MouseEventArgs e) {
            (sender as Button).DoDragDrop((sender as Button), DragDropEffects.Move);
        }

        ~Form1() {
            reader.Close();
            conn.Close();
        }
    }
}
