﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using MySql.Data.MySqlClient;
using System.IO;
//using WMPLib;

// Server=localhost\SQLEXPRESS;Database=master;Trusted_Connection=True;

namespace CashSys {
    public partial class StoreForm : Form {
        private Button[] pImages;
        private List<List<Control>> products;
        public static List<List<Control>> boughtProducts = new List<List<Control>>(); // keeping track of bought products to optimize time complexity
        private MySqlConnection conn = AuthDB.connectDB();
        private MySqlCommand command;
        private MySqlDataReader reader;
        private MemoryStream ms;
        private Customer c = new Customer();
        private int len;
        private ulong total = 0; // total price
        private Random rnd = new Random();
        private Dictionary<string, int> quantity_map; // using to simulate change in quantity of products (changes are made in RAM not DB)
        //private WindowsMediaPlayer myplayer = new WindowsMediaPlayer(); // to play soundeffects

        public StoreForm() {
            //m.getTickets().setTickets(new Dictionary<ushort, ushort>() {{1, 1},
            //                                                            {2, 0},
            //                                                            {5, 0},
            //                                                            {10, 0},
            //                                                            {20, 0},
            //                                                            {50, 1},
            //                                                            {100, 1},
            //                                                            {200, 0}});
            //m.getTickets().setTickets(1000);
            c.getTickets().setTickets(1000);
            InitializeComponent();
            displayProducts();
            addScrollBar();
            enableDrag();
            lbl_solde_value.Text = c.getTickets().getTotalAmount().ToString() + " MAD";
            //MessageBox.Show(Program.m.getTickets().getTotalAmount().ToString());
            Program.store = this;
        }

        private void displayProducts() {
            if (conn != null) {
                command = new MySqlCommand("SELECT COUNT(1) FROM Produit", conn);
                MySqlDataReader reader = command.ExecuteReader();
                
                while (reader.Read())
                    len = Convert.ToInt32(reader[0].ToString());

                quantity_map = new Dictionary<string, int>(len);
                products = new List<List<Control>>(len);
                pImages = new Button[len];
                reader.Close();
                command = new MySqlCommand("SELECT image, name, price, quantite FROM Produit", conn);

                reader = command.ExecuteReader();
                
                while (reader.Read()) {
                    len--;
                    Label name = new Label();
                    Label price = new Label();
                    int quantite = Convert.ToInt32(reader["quantite"].ToString());

                    if (quantite > 0) {
                        quantity_map.Add(reader["name"].ToString(), quantite);

                        pImages[len] = new Button();
                        pImages[len].MouseDown += button1_MouseDown;

                        pImages[len].Tag = reader["name"].ToString(); // used to detect which product is returned to stock
                        pImages[len].Width = 100;
                        pImages[len].Height = 100;
                        pImages[len].BackgroundImageLayout = ImageLayout.Stretch;


                        if (reader["image"] != null && !DBNull.Value.Equals(reader["image"])) {
                            ms = new MemoryStream((byte[])reader["image"]);
                            if (ms != null && ms.Length > 0)
                                pImages[len].BackgroundImage = Image.FromStream(ms);
                            else
                                pImages[len].BackgroundImage = null;

                            //pImages[len].BackgroundImage = Image.FromStream(ms);
                        }
                        //if (reader["image"].ToString() != "")
                        //    pImages[len].BackgroundImage = Image.FromFile(reader["image"].ToString());

                        name.Text = reader["name"].ToString();
                        price.Text = reader["price"].ToString() + " MAD";

                        products.Add(new List<Control> { pImages[len], name, price });
                        productsPanel.Controls.Add(pImages[len]);
                        productsPanel.Controls.Add(name);
                        productsPanel.Controls.Add(price);
                    }
                }
                reader.Close();
            }
        }

        private void addScrollBar() {
            productsPanel.AutoScroll = false;
            productsPanel.HorizontalScroll.Enabled = false;
            productsPanel.HorizontalScroll.Visible = false;
            productsPanel.HorizontalScroll.Maximum = 0;
            productsPanel.AutoScroll = true;

            boughtPanel.AutoScroll = false;
            boughtPanel.HorizontalScroll.Enabled = false;
            boughtPanel.HorizontalScroll.Visible = false;
            boughtPanel.HorizontalScroll.Maximum = 0;
            boughtPanel.AutoScroll = true;
        }

        private void button1_MouseDown(object sender, MouseEventArgs e) {
            (sender as Button).DoDragDrop((sender as Button), DragDropEffects.Move);
        }

        private void panel_DragEnter(object sender, DragEventArgs e) {
            e.Effect = DragDropEffects.Move;
        }

        private void panel_DragDrop(object sender, DragEventArgs e) {
            //Moving: ((Button)e.Data.GetData(typeof(Button))).Parent = (sender as Panel); 
            

            if ((((Button)e.Data.GetData(typeof(Button))).Parent) == productsPanel) {
                // Duplicate or move ?

                int quantite = 1; // to solve null problem in comparaison test

                Control c = e.Data.GetData(e.Data.GetFormats()[0]) as Control;
                quantity_map.TryGetValue(c.Tag.ToString(), out quantite);

                if (quantite > 1) { // need to decrease quanitity
                    // Duplicating
                    // rnd for creating random id for dynamic button (the one who will be added to boughtPanel)
                    Button btn = new Button();
                    btn.MouseDown += button1_MouseDown;
                    btn.Name = "Button" + rnd.Next();
                    btn.Size = c.Size;
                    btn.Tag = c.Tag; // will be useful
                    btn.BackgroundImageLayout = ImageLayout.Stretch;
                    btn.BackgroundImage = c.BackgroundImage;
                    if (c != null) {
                        btn.Text = c.Text;
                        btn.Location = scannerPanel.PointToClient(new Point(e.X, e.Y));
                        boughtPanel.Controls.Add(btn);
                    }
                }
                else {
                    // Moving and hiding data {price + name}
                    Button b = ((Button)e.Data.GetData(typeof(Button)));
                    b.Parent = boughtPanel;
                    
                    foreach (List<Control> l in products) {
                        if (l[0].Tag == b.Tag) {
                            l[1].Parent = null;
                            l[2].Parent = null;
                            break;
                        }
                    } 
                }


                // decreasing quantity 
                //using (conn = AuthDB.connectDB()) {
                //    try {
                //        command.CommandText = "UPDATE Produit SET quantity = quantity - 1 WHERE name = @name";
                //        command.Parameters.AddWithValue("@name", c.Tag);
                //        command.ExecuteNonQuery();
                //    }
                //    catch (Exception ex) {
                //        MessageBox.Show(ex.Message);
                //    }
                //}

                // decreasing quantity: simulation
                foreach (string tag in quantity_map.Keys ) {
                    if(tag == c.Tag.ToString()) {
                        quantity_map[tag]--;
                        break;
                    }
                }

                //myplayer.URL = "C:\\Users\\shetsecure\\source\\repos\\CashSystem - Copy\\CashSystem\\Resources\\mp3\\ShortBeepSoundEffect.mp3";
                //myplayer.controls.play();
                // compulsive memory consumer

                // editing total label               
                foreach (List<Control> l in products) {
                    if (l[0].Tag == ((Button)e.Data.GetData(typeof(Button))).Tag) {
                        try {
                            string[] tokens = l[2].Text.Split(' ');
                            total += Convert.ToUInt64(tokens[0]);
                        }
                        catch (Exception ex) {
                            // do nothing
                        }
                        boughtProducts.Add(l); // adding the list to boughtProducts list
                        break;
                    }
                }
            }
            else { // return to stock
                Button b = ((Button)e.Data.GetData(typeof(Button)));
                
                // need to increase quantity
                foreach (string tag in quantity_map.Keys) {
                    if (tag == b.Tag.ToString()) {
                        if (quantity_map[tag] > 0) // there still is a button representing the product in the productsPanel
                            b.Parent = null;
                        else {
                            b.Parent = productsPanel;
                        }
                        quantity_map[tag]++;

                        break;
                    }
                }

                // problem in showing details when having multiple instance of a product
                // the way it works, is while dragging the product to the boughtPanel, we duplicate the button
                // so we're creating a new button that doesn't exist in the list of List<Control> that we have
                // hence this test below will fail for any product with multiple instance
                // unless the user returns the last button, because it exists in the list of controls.

                // potential Solution:
                // while duplicating we can add a list that have the details along with the button -> memory complexity -> high
                // BEST WORKING SOLUTION: instead of comparing the reference to the product(button), we compare the Tag attribute
                // because even duplicated new buttons have the same Tag
                // display product's name and price after return it to productsPanel

                foreach (List<Control> l in products) {
                    if (l[0].Tag == b.Tag) {
                        l[1].Parent = productsPanel;
                        l[2].Parent = productsPanel;
                        try {
                            string[] tokens = l[2].Text.Split(' ');
                            total -= Convert.ToUInt64(tokens[0]);
                        }
                        catch (Exception ex) {
                            // do nothing
                        }

                        boughtProducts.Remove(l); // removing the product from boughtProduct List
                        break;
                    }
                }
            }

            lbl_total.Text = total.ToString();
        }

        public ulong getTotal() {
            return total;
        }

        private void enableDrag() {
            productsPanel.DragEnter += panel_DragEnter;
            scannerPanel.DragEnter += panel_DragEnter;

            //productsPanel.DragDrop += panel_DragDrop;
            scannerPanel.DragDrop += panel_DragDrop;
            boughtPanel.DragDrop += panel_DragDrop;
        }

        private ulong getCustomerSolde() {
            string[] tokens = lbl_solde_value.Text.Split(' ');

            return Convert.ToUInt64(tokens[0]);
        }

        private void btn_pay_Click(object sender, EventArgs e) {
            // getting all buttons
            /*foreach (var p in boughtPanel.Controls.OfType<Button>()) {
               // we need to search the product in the list for each one --> O(N^2)
               // + requesting database for the price and name --> O(1) (time consuming) --> for each one: O(N)
               // all products are now in the boughtProducts list
            }*/

            int len = boughtProducts.Count;
            ulong solde = getCustomerSolde();

            if (solde > total) {
                if (len > 0) {
                    bool confirmed = (MessageBox.Show("Etes-vous sure d'effectuer l'achat ? total = " + total, "Confirmer votre Achat",
                                MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1) == DialogResult.Yes);

                    if (confirmed) {
                        Paying p = new Paying(); // Payed form that displays the change and get the bill
                        Label[] change_lbls = p.GetChangeLabels();

                        //if(c.getTickets().express((ulong)total + 1)) { // only give tickets needed
                        //    c.pay(m, t, (ulong)total + 1); // charges
                        //    lbl_solde_value.Text = c.getTickets().getTotalAmount().ToString() + " MAD";
                        //    p.Show();
                        //    MessageBox.Show("solde de machine = " + m.getTickets().getTotalAmount());
                        //}

                        //if(c.pay(m, total)) {
                        //    lbl_solde_value.Text = c.getTickets().getTotalAmount().ToString() + " MAD";
                        //    p.Show();
                        //    MessageBox.Show("solde de machine = " + m.getTickets().getTotalAmount());
                        //}
                        //else {
                        //    MessageBox.Show("Cannot express the given amount with the available tickets of the customer");
                        //}
                        Dictionary<ushort, ushort> tickets_given_by_c, tickets_given_by_m;

                        // 

                        //MessageBox.Show(Program.m.getTickets().getTotalAmount().ToString());

                        if (c.getTickets().pay(total, out tickets_given_by_c)) {
                            ulong total_given_by_c = Convert.ToUInt64(tickets_given_by_c[1] + tickets_given_by_c[2] * 2 + tickets_given_by_c[5] * 5 + tickets_given_by_c[10] * 10 + tickets_given_by_c[20] * 20
                            + tickets_given_by_c[50] * 50 + tickets_given_by_c[100] * 100 + tickets_given_by_c[200] * 200);

                            Program.m.getTickets().addTickets(tickets_given_by_c);
                            ulong diff = total_given_by_c - total;

                            //MessageBox.Show(total_given_by_c.ToString());

                            if (Program.m.getTickets().pay(diff, out tickets_given_by_m, true)) {
                                c.getTickets().addTickets(tickets_given_by_m);
                                lbl_solde_value.Text = c.getTickets().getTotalAmount().ToString() + " MAD";
                                
                                int i = 0;
                                var map_lbl_pic = p.getDict(); // dict that links lbls with it's picture
                                foreach(var index in tickets_given_by_m.Keys) { 
                                    if (tickets_given_by_m[index] == 0) {
                                        change_lbls[i].Hide();
                                        map_lbl_pic[change_lbls[i]].Hide();
                                    }
                                    change_lbls[i].Text = tickets_given_by_m[index].ToString();
                                    
                                    i++;   
                                }
                                
                                p.Show();
                                //MessageBox.Show("solde de machine = " + Program.m.getTickets().getTotalAmount());
                                //Program.initForm.updateTexts();
                            }
                            else {
                                Dictionary<ushort, ushort> dummy;
                                c.getTickets().addTickets(tickets_given_by_c);
                                Program.m.getTickets().pay(total_given_by_c, out dummy, true); // give back the money (decreasing)
                                //m.getTickets().decreaseTickets(tickets_given_by_m); doesn't work properly, bad algo
                                MessageBox.Show("The machine can't express the change with the available tickets");
                                //MessageBox.Show(Program.m.getTickets().getTotalAmount().ToString());
                            }
                            //c.getTickets().addTickets(m.giveChange(tickets_given_by_c, total).getTickets());

                           
                        }
                        else {
                            MessageBox.Show("Cannot express the given amount with the available tickets of the customer");
                        }

                    }
                }
                else
                    MessageBox.Show("Veuillez choisir un produit.");
            }
            else
                MessageBox.Show("Votre solde ne suffit pas pour effectuer cette operation.");

            // decrease quanities of each product
            // increase cash machine tickets
            // decrease customers tickets
            // express change with pictures
        }

        public List<Control> getBoughtProducts() {
            List<Control> boughtProducts = new List<Control>();
            boughtProducts.AddRange(boughtPanel.Controls.OfType<Control>());
            return boughtProducts;
        }
        
        ~StoreForm() {
            reader.Close();
            conn.Close();
            //myplayer.close();
            //Application.ExitThread();
            //Application.Exit();
            //Close();
        }
    }
}
