﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CashSys {
    public partial class BillForm : Form {
        private int row = 1;
        //private List<List<Control>> boughtProducts = StoreForm.boughtProducts;
        public BillForm() {
            InitializeComponent();

            panel.Controls.Add(new Label() { Text = "Nom du produit" }, 0, 0);
            panel.Controls.Add(new Label() { Text = "Prix unitaire" }, 1, 0);
            panel.Controls.Add(new Label() { Text = "Quantite" }, 2, 0);
            panel.Controls.Add(new Label() { Text = "Prix * Quantite" }, 3, 0);

            var boughtProducts = Program.store.getBoughtProducts();
            //var map = from x in boughtProducts
            //        group x by x into g
            //        let count = g.Count()
            //        orderby count descending
            //        select new { Value = g.Key.Tag.ToString(), Count = count };

            var map = boughtProducts.GroupBy(x => x.Tag)
            .Select(x => new {
                Count = x.Count(),
                Name = x.Key,
            })
            .OrderByDescending(x => x.Count);

            

            foreach (var product in map) {
                RowStyle temp = panel.RowStyles[panel.RowCount - 1];
                temp.Height = 10;
                int price = -1;

                // searching for the price
                foreach(var ll in StoreForm.boughtProducts) {
                    if(product.Name.ToString() == ll[1].Text) {
                        // extracting price -> removing MAD
                        string[] tokens = ll[2].Text.Split(' ');
                        price = int.Parse(tokens[0]);
                        break;
                    }
                }

                if (price > 0) {
                    panel.RowStyles.Add(new RowStyle(temp.SizeType, temp.Height));

                    panel.Controls.Add(new Label() { Text = product.Name.ToString() }, 0, panel.RowCount - 1);
                    panel.Controls.Add(new Label() { Text = price.ToString() }, 1, panel.RowCount - 1);
                    panel.Controls.Add(new Label() { Text = product.Count.ToString() }, 2, panel.RowCount - 1);
                    panel.Controls.Add(new Label() { Text = (price*product.Count).ToString() }, 3, row++);
                    panel.RowCount++;
                }
                // else something bad happened and we didn't find it -> weird
            }

            panel.RowCount--;
            lbl_total.Text = Program.store.getTotal().ToString() + " MAD";
        }
    }
}
