﻿using System;
using System.Collections.Generic;
using System.Linq;


namespace CashSys {
    public class Tickets {
        private Dictionary<ushort, ushort> tickets = new Dictionary<ushort, ushort>() {
            {1, 0},
            {2, 0},
            {5, 0},
            {10, 0},
            {20, 0},
            {50, 0},
            {100, 0},
            {200, 0}
        };

        public Tickets(Dictionary<ushort, ushort> d) {
            tickets = d;
        }

        public Tickets(ulong amount) {
            optimize(amount);
        }

        public void addTickets(Dictionary<ushort, ushort> t) {
            tickets[1] += t[1];
            tickets[2] += t[2];
            tickets[5] += t[5];
            tickets[10] += t[10];
            tickets[20] += t[20];
            tickets[50] += t[50];
            tickets[100] += t[100];
            tickets[200] += t[200];
        }

        public void decreaseTickets(Dictionary<ushort, ushort> t) { // fix this: DO NOT USE
            tickets[1] = (ushort) ( (tickets[1] - t[1]) > 0 ? (tickets[1] - t[1]) : tickets[1] );
            tickets[2] = (ushort) ( (tickets[2] - t[2]) > 0 ? (tickets[2] - t[2]) : tickets[2]);
            tickets[5] = (ushort) ( (tickets[5] - t[5]) > 0 ? (tickets[5] - t[5]) : tickets[5]);
            tickets[10] = (ushort) ( (tickets[10] - t[10]) > 0 ? (tickets[10] - t[10]) : tickets[10]);
            tickets[20] = (ushort) ( (tickets[20] - t[20]) > 0 ? (tickets[20] - t[20]) : tickets[20]);
            tickets[50] = (ushort) ( (tickets[50] - t[50]) > 0 ? (tickets[50] - t[50]) : tickets[50]);
            tickets[100] = (ushort) ((tickets[100] - t[100]) > 0 ? (tickets[100] - t[100]) : tickets[100]);
            tickets[200] = (ushort) ( (tickets[200] - t[200]) > 0 ? (tickets[200] - t[200]) : tickets[200]);           
        }

        public void decreaseTickets(Tickets _t) { 
            decreaseTickets(_t.getTickets());
        }

        public void setTickets(ulong amount) {
            optimize(amount);
        }

        public void setTickets(Dictionary<ushort, ushort> t) {
            tickets = t.ToDictionary(entry => entry.Key, entry => entry.Value);
        }

        public Dictionary<ushort, ushort> getTickets() {
            return tickets;
        }
        
        public ulong getTotalAmount() {
            return (ulong)(tickets[1] + tickets[2]*2 + tickets[5]*5 + tickets[10]*10 + tickets[20]*20
                            + tickets[50]*50 + tickets[100]*100 + tickets[200]*200);
        }

        // optimize will use the greedy algorithm
        public void optimize(ulong amount) {

            foreach (var ticket in tickets.Keys.ToList())
                tickets[ticket] = 0;

            while (amount > 0) {
                while (amount >= 200) {
                    amount -= 200; tickets[200]++;
                }
                while (amount >= 100) {
                    amount -= 100; tickets[100]++;
                }
                while (amount >= 50) {
                    amount -= 50; tickets[50]++;
                }
                while (amount >= 20) {
                    amount -= 20; tickets[20]++;
                }
                while (amount >= 10) {
                    amount -= 10; tickets[10]++;
                }
                while (amount >= 5) {
                    amount -= 5; tickets[5]++;
                }
                while (amount >= 2) {
                    amount -= 2; tickets[2]++;
                }
                while (amount >= 1) {
                    amount -= 1; tickets[1]++;
                }
            }
        }

        public bool express(ulong amount, out Tickets t) {
            // will be used mainly by the machine for change.
            // can you express amount with available tickets ?
            if (getTotalAmount() < amount) {
                t = null;
                return false;
            }

            t = new Tickets(amount);
            t.optimize(amount);
            return true;
        }

        // deprecated
        public bool pay_DEPRECATED(ulong amount, out Dictionary<ushort, ushort> minTotal) {    // deprecated  
            ulong total = getTotalAmount();
            ulong sum;
            minTotal = new Dictionary<ushort, ushort>() {
                                                        {1, 0},
                                                        {2, 0},
                                                        {5, 0},
                                                        {10, 0},
                                                        {20, 0},
                                                        {50, 0},
                                                        {100, 0},
                                                        {200, 0}
                                                    };

            if (total < amount) {
                minTotal = null;
                return false;
            }

            IEnumerable<ushort> enumT = tickets.Keys.AsEnumerable(); // to start from 200dh -> 1dh
            foreach (var index in enumT.Reverse()) {
                sum = 0;
                while (amount > 0) { //amount >= index
                    while (tickets[index] > 0) {
                        if (amount >= index) {
                            amount -= index;
                            tickets[index]--;
                            minTotal[index]++;
                        }
                        else
                            break;

                        foreach (var ii in tickets.Keys) {
                            if (ii == index)
                                break;

                            sum += (ulong)ii * tickets[ii];
                        }
                        if (sum < amount) // can't express it with smaller amounts, needs bigger ticket
                            goto nd;
                    }
                    break;
                }
            }

            nd:
            if(amount > 0) {
                foreach (var index in tickets.Keys) {
                    if(tickets[index] > 0 && index >= amount) {
                        tickets[index]--;
                        minTotal[index]++;
                        break;
                    }
                }
            }

            return true;
        } 

        public bool pay(ulong amount, out Dictionary<ushort, ushort> minOut, bool exact = false) {
            // copy
            var original_tickets = new Dictionary<ushort, ushort>() {
                                                        {1, 0},
                                                        {2, 0},
                                                        {5, 0},
                                                        {10, 0},
                                                        {20, 0},
                                                        {50, 0},
                                                        {100, 0},
                                                        {200, 0}
                                                    }; // maybe needs it, when using copy
            foreach (var index in tickets.Keys) original_tickets[index] = tickets[index];
            minOut = new Dictionary<ushort, ushort>() {
                                                        {1, 0},
                                                        {2, 0},
                                                        {5, 0},
                                                        {10, 0},
                                                        {20, 0},
                                                        {50, 0},
                                                        {100, 0},
                                                        {200, 0}
                                                    };
            ulong amount2 = amount;

            if (getTotalAmount() < amount) {
                minOut = null;
                return false;
            }

            IEnumerable<ushort> enumT = tickets.Keys.AsEnumerable(); // to start from 200dh -> 1dh
            foreach (var index in enumT.Reverse()) {
                while (amount > 0) { //amount >= index
                    while (tickets[index] > 0) {
                        if (amount >= index) {
                            amount -= index;
                            tickets[index]--;
                            minOut[index]++;
                        }
                        else
                            break;
                    }
                    break;
                }
            }

            
            if (amount > 0) {
                tickets = original_tickets;
                if (exact) {
                    minOut = null;
                    return false;
                }
                    

                // reset all vars
                amount = amount2;
                minOut = new Dictionary<ushort, ushort>() {
                                                        {1, 0},
                                                        {2, 0},
                                                        {5, 0},
                                                        {10, 0},
                                                        {20, 0},
                                                        {50, 0},
                                                        {100, 0},
                                                        {200, 0}
                                                    };
                
                ulong sum = 0;

                foreach (var index in enumT.Reverse()) {
                    while (tickets[index] > 0) {
                        //if (amount >= index) {
                            sum += index;
                            tickets[index]--;
                            minOut[index]++;
                            if (sum >= amount)
                                goto end;
                        //}
                        //else
                        //    break;
                    }
                }
            }

            end:
            return true;
        }
    }
}