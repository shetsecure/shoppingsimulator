﻿namespace CashSys {
    partial class MyForm {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MyForm));
            this.lbl_who = new System.Windows.Forms.Label();
            this.lbl_admin = new System.Windows.Forms.Label();
            this.lbl_cashier = new System.Windows.Forms.Label();
            this.lbl_customer = new System.Windows.Forms.Label();
            this.btn_customer = new System.Windows.Forms.Button();
            this.btn_cashier = new System.Windows.Forms.Button();
            this.btn_admin = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // lbl_who
            // 
            this.lbl_who.AutoSize = true;
            this.lbl_who.Font = new System.Drawing.Font("Microsoft Sans Serif", 28.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_who.Location = new System.Drawing.Point(281, 29);
            this.lbl_who.Name = "lbl_who";
            this.lbl_who.Size = new System.Drawing.Size(255, 44);
            this.lbl_who.TabIndex = 0;
            this.lbl_who.Text = "Who are you?";
            // 
            // lbl_admin
            // 
            this.lbl_admin.AutoSize = true;
            this.lbl_admin.Location = new System.Drawing.Point(134, 144);
            this.lbl_admin.Name = "lbl_admin";
            this.lbl_admin.Size = new System.Drawing.Size(36, 13);
            this.lbl_admin.TabIndex = 1;
            this.lbl_admin.Text = "Admin";
            // 
            // lbl_cashier
            // 
            this.lbl_cashier.AutoSize = true;
            this.lbl_cashier.Location = new System.Drawing.Point(376, 144);
            this.lbl_cashier.Name = "lbl_cashier";
            this.lbl_cashier.Size = new System.Drawing.Size(42, 13);
            this.lbl_cashier.TabIndex = 2;
            this.lbl_cashier.Text = "Cashier";
            // 
            // lbl_customer
            // 
            this.lbl_customer.AutoSize = true;
            this.lbl_customer.Location = new System.Drawing.Point(639, 144);
            this.lbl_customer.Name = "lbl_customer";
            this.lbl_customer.Size = new System.Drawing.Size(51, 13);
            this.lbl_customer.TabIndex = 3;
            this.lbl_customer.Text = "Customer";
            // 
            // btn_customer
            // 
            this.btn_customer.BackgroundImage = global::CashSys.Properties.Resources.customer;
            this.btn_customer.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btn_customer.Location = new System.Drawing.Point(563, 198);
            this.btn_customer.Name = "btn_customer";
            this.btn_customer.Size = new System.Drawing.Size(200, 100);
            this.btn_customer.TabIndex = 6;
            this.btn_customer.UseVisualStyleBackColor = true;
            this.btn_customer.Click += new System.EventHandler(this.btn_customer_Click);
            // 
            // btn_cashier
            // 
            this.btn_cashier.BackgroundImage = global::CashSys.Properties.Resources.male_fast_food_workers_collection_008;
            this.btn_cashier.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btn_cashier.Location = new System.Drawing.Point(300, 198);
            this.btn_cashier.Name = "btn_cashier";
            this.btn_cashier.Size = new System.Drawing.Size(200, 100);
            this.btn_cashier.TabIndex = 5;
            this.btn_cashier.UseVisualStyleBackColor = true;
            // 
            // btn_admin
            // 
            this.btn_admin.BackgroundImage = global::CashSys.Properties.Resources.AA;
            this.btn_admin.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btn_admin.Location = new System.Drawing.Point(52, 198);
            this.btn_admin.Name = "btn_admin";
            this.btn_admin.Size = new System.Drawing.Size(200, 100);
            this.btn_admin.TabIndex = 4;
            this.btn_admin.UseVisualStyleBackColor = true;
            this.btn_admin.Click += new System.EventHandler(this.btn_admin_Click);
            // 
            // MyForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.btn_customer);
            this.Controls.Add(this.btn_cashier);
            this.Controls.Add(this.btn_admin);
            this.Controls.Add(this.lbl_customer);
            this.Controls.Add(this.lbl_cashier);
            this.Controls.Add(this.lbl_admin);
            this.Controls.Add(this.lbl_who);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "MyForm";
            this.Text = "Cash System";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lbl_who;
        private System.Windows.Forms.Label lbl_admin;
        private System.Windows.Forms.Label lbl_cashier;
        private System.Windows.Forms.Label lbl_customer;
        private System.Windows.Forms.Button btn_admin;
        private System.Windows.Forms.Button btn_cashier;
        private System.Windows.Forms.Button btn_customer;
    }
}