﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CashSys {
    public partial class MyForm : Form {
        
        public MyForm() {
            InitializeComponent();
            Program.myform = this;
            
        }

        private void btn_admin_Click(object sender, EventArgs e) {
            //if (Program.auth == null) {
                AuthForm a = new AuthForm();
                a.Show();
            //}
            //else
            //    Program.auth.ShowDialog();
        }

        private void btn_customer_Click(object sender, EventArgs e) {
            StoreForm c = new StoreForm();
            c.Show();
            //c.ShowDialog();
        }

        ~MyForm() {
            //Application.ExitThread();
            //Application.Exit();
            //Close();
        }
    }
}
