﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using MySql.Data.MySqlClient;
using System.IO;

namespace CashSys {
    public partial class AdminForm : Form { 
        string imgPath = "";
        double price = 0;
        public static List<Product> AllProducts { get; } = new List<Product>();
        public static int len = 0;
        MySqlConnection conn = AuthDB.connectDB();
        string fileName;
        bool add_clicked = false;
        bool modify_clicked = false;

        public AdminForm() {

            if (conn != null) {
                InitializeComponent();
                btn_selectImg.Enabled = false;
                btn_cancel.Enabled = false;
                btn_save.Enabled = false;
                populate_comboBox();
                showProductData();
                editable(false);
            }
            else {
                MessageBox.Show("Probleme de connexion a la base de donnees.");
                Application.Exit();
                Application.ExitThread();
                Close();
            }
        }

        private void populate_comboBox() {
            try {
                string query = "select name from Produit";
                MySqlDataAdapter da = new MySqlDataAdapter(query, conn);
                DataSet ds = new DataSet();
                da.Fill(ds, "Name");
                cmbProductName.DisplayMember = "name";
                cmbProductName.DataSource = ds.Tables["Name"];
            }
            catch (Exception ex) {
                // Add error to log file
                MessageBox.Show(ex.Message);
                MessageBox.Show("Error occured!");
            }
        }

        private void btn_selectImg_Click(object sender, EventArgs e) {
            OpenFileDialog fileBrowser = new OpenFileDialog();
            fileBrowser.Filter = "Image files | *.jpg; *.jpeg; *.png";
            restart:

            if(fileBrowser.ShowDialog() == DialogResult.OK) {
                fileName = fileBrowser.FileName;
                int index = fileName.LastIndexOf('.') + 1;
                int len = fileName.Length - index;
                string extension = fileName.Substring(index, len);

                List<String> allowedExtensions = new List<string> {"png", "jpg", "jpeg"};
                bool found = false;

                foreach(string a in allowedExtensions) 
                    if(extension == a) {
                        found = true;
                        break;
                    }

                if(found) {
                    //btn_cancel.Enabled = true;
                    imgPath = fileName;
                    pictureBox1.BackgroundImage = Image.FromFile(fileName);
                }
                else {
                    //btn_cancel.Enabled = false;
                    MessageBox.Show("File not allowed ! Please choose a picture (png, jpg or jpeg).");
                    goto restart;
                }
            }
        }

        /* TO INSERT NEW PRODUCT USE THIS
         private void btn_addProduct_Click(object sender, EventArgs e) {
            //string name = txt_productName.Text;
            ulong cab = (ulong) num_productCab.Value;

            //if(imgPath != "") {
                //MyMySqlCommand command = new SqlCommand ($"INSERT INTO Produit(cab, image, name, price) Values({cab}, '{fileName}', '{name}', {price})", conn);
                //command.ExecuteNonQuery();
                //AllProducts.Add(new Product(name, price, cab, ));
                //len++;
                MessageBox.Show("New Product added successfully!");
               // reset();
            //}
        }
             */

        private void txt_productPrice_TextChanged(object sender, EventArgs e) {
            if (add_clicked) {
                if (Double.TryParse(txt_productPrice.Text, out price) && price > 0) {
                    btn_save.Enabled = true;
                    lbl_error.Text = "";
                }
                else {
                    btn_save.Enabled = false;
                    lbl_error.Text = "Please enter a positive number in price field.";
                    lbl_error.BackColor = Color.Red;
                }
            }
        }

        //pictureBox1.BackgroundImage = Image.FromFile(fileName);

        private string getSelectedProduct() {
            if (cmbProductName.SelectedItem != null) {
                DataRowView drv = cmbProductName.SelectedItem as DataRowView;
                //showProductImage();
                return drv.Row["Name"].ToString();
            }
            else {
                return "Empty";
            }
        }

        private void showProductImage() {
            string productName = getSelectedProduct();
            string sql = "SELECT image FROM Produit WHERE name = '" + productName + "'";

            using (conn = AuthDB.connectDB()) {
                MySqlCommand cmd = new MySqlCommand(sql, conn);
                MemoryStream ms = null;

                try {
                    using (var reader = cmd.ExecuteReader())
                        while (reader.Read()) {
                            if (reader["image"] != null && !DBNull.Value.Equals(reader["image"])) 
                                ms = new MemoryStream((byte[])reader["image"]);
                            break;
                        }

                    //string path = (string) cmd.ExecuteScalar();
                    //if (path != "")
                    //    pictureBox1.BackgroundImage = Image.FromFile(path);
                    //else
                    //    pictureBox1.BackgroundImage = null;

                    if (ms != null && ms.Length > 0) 
                        pictureBox1.BackgroundImage = Image.FromStream(ms);
                    else
                        pictureBox1.BackgroundImage = null;
                }
                catch (Exception ex) {
                    MessageBox.Show(ex.Message);
                }
            }
        }

        private void showProductData() {
            showProductImage();
            string productName = getSelectedProduct();
            string sql = "SELECT price FROM Produit WHERE name = '" + productName + "'";

            using (conn = AuthDB.connectDB()) {
                MySqlCommand cmd = new MySqlCommand(sql, conn);
              
                try {
                    double price = Convert.ToDouble(cmd.ExecuteScalar());
                    //num_productCab.Value = Convert.ToDecimal(cab);
                    txt_productPrice.Text = price.ToString();
                    //num_productCab.Value = 100;
                }
                catch (Exception ex) {
                    MessageBox.Show(ex.Message);
                }
            }

            sql = "SELECT cab, quantite FROM Produit WHERE name = '" + productName + "'";

            using (conn = AuthDB.connectDB()) {
                MySqlCommand cmd = new MySqlCommand(sql, conn);

                try {
                    using (var reader = cmd.ExecuteReader())
                        if (reader.Read()) {
                            decimal cab = Convert.ToDecimal(reader[0]);
                            decimal quantite = Convert.ToDecimal(reader[1]);
                            num_productCab.Value = cab;
                            num_quantite.Value = quantite;
                        }

                }
                catch (Exception ex) {
                    MessageBox.Show(ex.Message);
                }

                //try {
                //    decimal cab = Convert.ToDecimal(cmd.ExecuteScalar());
                //    num_productCab.Value = cab;
                //}
                //catch (Exception ex) {
                //    MessageBox.Show(ex.Message);
                //}
            }
        }

        private void editable(bool status) {
            if(status == true) {
                btn_selectImg.Enabled = true;
                btn_cancel.Enabled = true;
                btn_save.Enabled = true;
                btn_add.Enabled = false;
                btn_delete.Enabled = false;
                btn_edit.Enabled = false;

                num_productCab.Enabled = true;
                num_quantite.Enabled = true;
                txt_productPrice.Enabled = true;
                txt_productName.Visible = true;
                cmbProductName.Visible = false;
            }
            else {
                btn_selectImg.Enabled = false;
                btn_cancel.Enabled = false;
                btn_save.Enabled = false;
                btn_add.Enabled = true;
                btn_delete.Enabled = true;
                btn_edit.Enabled = true;

                num_productCab.Enabled = false;
                num_quantite.Enabled = false;
                txt_productPrice.Enabled = false;
                txt_productName.Visible = false;
                cmbProductName.Visible = true;
                lbl_error.Text = "";
            }
        }

        private void flush() {
            num_productCab.Value = 0;
            txt_productPrice.Text = "";
            pictureBox1.BackgroundImage = null;
            txt_productName.Text = "";
        }

        private void btn_add_Click(object sender, EventArgs e) {
            editable(true);
            add_clicked = true;
            flush();
        }

        private void btn_edit_Click(object sender, EventArgs e) {
            editable(true);
            modify_clicked = true;
            txt_productName.Text = getSelectedProduct();
        }

        private void btn_delete_Click(object sender, EventArgs e) {
            editable(false);
            var confirmResult = MessageBox.Show("Êtes-vous sûr de vouloir supprimer cet article?", "Confirmation de la suppression !!", MessageBoxButtons.YesNo);

            if (confirmResult == DialogResult.Yes) {
                try {
                    using ( conn = AuthDB.connectDB())
                    using (var cmd = conn.CreateCommand()) {
                        cmd.CommandText = "DELETE FROM Produit WHERE cab = @cab";
                        cmd.Parameters.AddWithValue("@cab", num_productCab.Value);
                        cmd.ExecuteNonQuery();
                        //MessageBox.Show("article supprimé");
                        using (conn = AuthDB.connectDB()) {
                            populate_comboBox();
                            showProductData();
                        }
                    }
                }
                catch (Exception ex) {
                    MessageBox.Show(ex.Message);
                }
            }
        }

        private void btn_save_Click(object sender, EventArgs e) {
            editable(false);

            ImageConverter imgConv = new ImageConverter();
            byte[] img = new byte[0];
            //pictureBox1.BackgroundImage = null;
            if (pictureBox1.BackgroundImage != null)
                img = (byte[])imgConv.ConvertTo(pictureBox1.BackgroundImage, Type.GetType("System.Byte[]"));

            if (add_clicked) {
                string name = txt_productName.Text;
                int price = Convert.ToInt32(txt_productPrice.Text);
                string sql = "SELECT COUNT(*) FROM Produit WHERE name = '" + name + "'";

                using (conn = AuthDB.connectDB()) {
                    MySqlCommand cmd = new MySqlCommand(sql, conn);
                    try {
                        int res = Convert.ToInt32(cmd.ExecuteScalar().ToString());
                        if (res <= 0 && name != "") {
                            MySqlCommand command = null;

                            if (pictureBox1.BackgroundImage != null) {
                                command = new MySqlCommand($"INSERT INTO Produit(cab, image, name, price, quantite) Values({num_productCab.Value}, @image, '{name}', {price}, {num_quantite.Value})", conn);
                                command.Parameters.AddWithValue("@image", img);
                            }
                            else
                                command = new MySqlCommand($"INSERT INTO Produit(cab, name, price, quantite) Values({num_productCab.Value}, '{name}', {price}, {num_quantite.Value})", conn);
                            try {
                                command.ExecuteNonQuery();
                                populate_comboBox();
                                cmbProductName.SelectedIndex = cmbProductName.FindString(name);
                               
                            }
                            catch (SqlException ex) {
                                if (ex.Message.StartsWith("Violation of PRIMARY"))
                                    MessageBox.Show("Bar code can't be duplicated. Please enter a new one.");
                                else
                                    MessageBox.Show(ex.Message);
                                editable(true);
                            }
                        }
                        else {
                            if(name == "")
                                MessageBox.Show("Please enter the product name.");
                            else
                                MessageBox.Show("This product already exists. Please choose another name");
                            editable(true);
                        }
                    }
                    catch (Exception ex) {
                        MessageBox.Show(ex.Message);
                    }
                }
            }
            else if (modify_clicked) {
                string name = txt_productName.Text;
                //num_productCab.Enabled = false;
                Int64 cab = (Int64) num_productCab.Value;
                //string sql = "SELECT COUNT(*) FROM Produit WHERE name = '" + name + "'";

                try {
                    using (conn = AuthDB.connectDB()) {
                        MySqlCommand cmd = new MySqlCommand();
                        //int res = (int) cmd.ExecuteScalar();
                        
                        using (cmd = conn.CreateCommand()) {

                            cmd.CommandText = "UPDATE Produit SET name = @name, price = @price, image = @image, quantite = @quantite WHERE cab = @cab ";
                            cmd.Parameters.AddWithValue("@cab", cab);
                            cmd.Parameters.AddWithValue("@name", name);
                            cmd.Parameters.AddWithValue("@price", Convert.ToDouble(txt_productPrice.Text));
                            cmd.Parameters.AddWithValue("@image", img);
                            cmd.Parameters.AddWithValue("@quantite", num_quantite.Value);
                            cmd.ExecuteNonQuery();
                            //MessageBox.Show("article supprimé");
                            using (conn = AuthDB.connectDB()) {
                                populate_comboBox();
                                showProductData();
                                cmbProductName.SelectedIndex = cmbProductName.FindString(name);
                            }
                        }
                    }
                }
                catch (Exception ex) {
                    MessageBox.Show(ex.Message);
                }
            }
        }

        private void btn_cancel_Click(object sender, EventArgs e) {
            editable(false);
            showProductData();
            add_clicked = false;
            modify_clicked = false;
        }

        private void cmbProductName_SelectedIndexChanged(object sender, EventArgs e) {
            using(conn = AuthDB.connectDB()) {
                showProductData();
            }
        }

        ~AdminForm() {
            conn.Close();
            //Application.Exit();
            //Application.ExitThread();
            //Close();
        }
    }
}
