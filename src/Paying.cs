﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CashSys {
    public partial class Paying : Form {
        public Dictionary<Label, PictureBox> lbl_pic = new Dictionary<Label, PictureBox>();

        public Paying() {
            InitializeComponent();
            populate_dict();
        }

        public Label[] GetChangeLabels() {
            Label[] lbls = new Label[] { lbl1, lbl2, lbl5, lbl10, lbl20, lbl50, lbl100, lbl200 };

            return lbls;
        }

        private void populate_dict() {
            lbl_pic.Add(lbl1, pictureBox1);
            lbl_pic.Add(lbl2, pictureBox2);
            lbl_pic.Add(lbl5, pictureBox8);
            lbl_pic.Add(lbl10, pictureBox7);
            lbl_pic.Add(lbl20, pictureBox6);
            lbl_pic.Add(lbl50, pictureBox5);
            lbl_pic.Add(lbl100, pictureBox4);
            lbl_pic.Add(lbl200, pictureBox3);
        }

        public Dictionary<Label, PictureBox> getDict() {
            return lbl_pic;
        }

        ~Paying() {
            //Application.ExitThread();
            //Application.Exit();
            Close();
        }

        private void btn_bill_Click(object sender, EventArgs e) {
            var bill = new BillForm();
            bill.Show();
        }
    }
}
