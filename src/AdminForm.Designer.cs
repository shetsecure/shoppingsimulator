﻿namespace CashSys {
    partial class AdminForm {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.components = new System.ComponentModel.Container();
            this.lbl_addNewProduct = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.btn_selectImg = new System.Windows.Forms.Button();
            this.lbl_productName = new System.Windows.Forms.Label();
            this.lbl_productPrice = new System.Windows.Forms.Label();
            this.lbl_cab = new System.Windows.Forms.Label();
            this.txt_productPrice = new System.Windows.Forms.TextBox();
            this.btn_cancel = new System.Windows.Forms.Button();
            this.num_productCab = new System.Windows.Forms.NumericUpDown();
            this.btn_add = new System.Windows.Forms.Button();
            this.btn_edit = new System.Windows.Forms.Button();
            this.btn_delete = new System.Windows.Forms.Button();
            this.btn_save = new System.Windows.Forms.Button();
            this.productBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.productBindingSource1 = new System.Windows.Forms.BindingSource(this.components);
            this.cmbProductName = new System.Windows.Forms.ComboBox();
            this.txt_productName = new System.Windows.Forms.TextBox();
            this.lbl_error = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.num_quantite = new System.Windows.Forms.NumericUpDown();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.num_productCab)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.productBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.productBindingSource1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.num_quantite)).BeginInit();
            this.SuspendLayout();
            // 
            // lbl_addNewProduct
            // 
            this.lbl_addNewProduct.AutoSize = true;
            this.lbl_addNewProduct.Font = new System.Drawing.Font("Microsoft Sans Serif", 30F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_addNewProduct.Location = new System.Drawing.Point(232, 25);
            this.lbl_addNewProduct.Name = "lbl_addNewProduct";
            this.lbl_addNewProduct.Size = new System.Drawing.Size(421, 46);
            this.lbl_addNewProduct.TabIndex = 0;
            this.lbl_addNewProduct.Text = "Products management";
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox1.Location = new System.Drawing.Point(12, 99);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(198, 119);
            this.pictureBox1.TabIndex = 1;
            this.pictureBox1.TabStop = false;
            // 
            // btn_selectImg
            // 
            this.btn_selectImg.Location = new System.Drawing.Point(43, 224);
            this.btn_selectImg.Name = "btn_selectImg";
            this.btn_selectImg.Size = new System.Drawing.Size(134, 23);
            this.btn_selectImg.TabIndex = 2;
            this.btn_selectImg.Text = "Select Product Image";
            this.btn_selectImg.UseVisualStyleBackColor = true;
            this.btn_selectImg.Click += new System.EventHandler(this.btn_selectImg_Click);
            // 
            // lbl_productName
            // 
            this.lbl_productName.AutoSize = true;
            this.lbl_productName.Location = new System.Drawing.Point(265, 99);
            this.lbl_productName.Name = "lbl_productName";
            this.lbl_productName.Size = new System.Drawing.Size(38, 13);
            this.lbl_productName.TabIndex = 3;
            this.lbl_productName.Text = "Name:";
            // 
            // lbl_productPrice
            // 
            this.lbl_productPrice.AutoSize = true;
            this.lbl_productPrice.Location = new System.Drawing.Point(265, 140);
            this.lbl_productPrice.Name = "lbl_productPrice";
            this.lbl_productPrice.Size = new System.Drawing.Size(34, 13);
            this.lbl_productPrice.TabIndex = 4;
            this.lbl_productPrice.Text = "Price:";
            // 
            // lbl_cab
            // 
            this.lbl_cab.AutoSize = true;
            this.lbl_cab.Location = new System.Drawing.Point(265, 186);
            this.lbl_cab.Name = "lbl_cab";
            this.lbl_cab.Size = new System.Drawing.Size(54, 13);
            this.lbl_cab.TabIndex = 5;
            this.lbl_cab.Text = "Bar Code:";
            // 
            // txt_productPrice
            // 
            this.txt_productPrice.Location = new System.Drawing.Point(361, 140);
            this.txt_productPrice.Name = "txt_productPrice";
            this.txt_productPrice.Size = new System.Drawing.Size(158, 20);
            this.txt_productPrice.TabIndex = 7;
            this.txt_productPrice.TextChanged += new System.EventHandler(this.txt_productPrice_TextChanged);
            // 
            // btn_cancel
            // 
            this.btn_cancel.Location = new System.Drawing.Point(668, 403);
            this.btn_cancel.Name = "btn_cancel";
            this.btn_cancel.Size = new System.Drawing.Size(75, 23);
            this.btn_cancel.TabIndex = 17;
            this.btn_cancel.Text = "Annuler";
            this.btn_cancel.UseVisualStyleBackColor = true;
            this.btn_cancel.Click += new System.EventHandler(this.btn_cancel_Click);
            // 
            // num_productCab
            // 
            this.num_productCab.Location = new System.Drawing.Point(361, 184);
            this.num_productCab.Maximum = new decimal(new int[] {
            268435455,
            1042612833,
            542101086,
            0});
            this.num_productCab.Name = "num_productCab";
            this.num_productCab.Size = new System.Drawing.Size(158, 20);
            this.num_productCab.TabIndex = 18;
            // 
            // btn_add
            // 
            this.btn_add.Location = new System.Drawing.Point(289, 403);
            this.btn_add.Name = "btn_add";
            this.btn_add.Size = new System.Drawing.Size(75, 23);
            this.btn_add.TabIndex = 19;
            this.btn_add.Text = "Ajouter";
            this.btn_add.UseVisualStyleBackColor = true;
            this.btn_add.Click += new System.EventHandler(this.btn_add_Click);
            // 
            // btn_edit
            // 
            this.btn_edit.Location = new System.Drawing.Point(384, 403);
            this.btn_edit.Name = "btn_edit";
            this.btn_edit.Size = new System.Drawing.Size(75, 23);
            this.btn_edit.TabIndex = 20;
            this.btn_edit.Text = "Modifier";
            this.btn_edit.UseVisualStyleBackColor = true;
            this.btn_edit.Click += new System.EventHandler(this.btn_edit_Click);
            // 
            // btn_delete
            // 
            this.btn_delete.Location = new System.Drawing.Point(476, 403);
            this.btn_delete.Name = "btn_delete";
            this.btn_delete.Size = new System.Drawing.Size(75, 23);
            this.btn_delete.TabIndex = 21;
            this.btn_delete.Text = "Supprimer";
            this.btn_delete.UseVisualStyleBackColor = true;
            this.btn_delete.Click += new System.EventHandler(this.btn_delete_Click);
            // 
            // btn_save
            // 
            this.btn_save.Location = new System.Drawing.Point(570, 403);
            this.btn_save.Name = "btn_save";
            this.btn_save.Size = new System.Drawing.Size(75, 23);
            this.btn_save.TabIndex = 22;
            this.btn_save.Text = "Enregistrer";
            this.btn_save.UseVisualStyleBackColor = true;
            this.btn_save.Click += new System.EventHandler(this.btn_save_Click);
            // 
            // productBindingSource
            // 
            this.productBindingSource.DataMember = "Product";
            // 
            // productBindingSource1
            // 
            this.productBindingSource1.DataMember = "Product";
            // 
            // cmbProductName
            // 
            this.cmbProductName.FormattingEnabled = true;
            this.cmbProductName.Location = new System.Drawing.Point(361, 92);
            this.cmbProductName.Name = "cmbProductName";
            this.cmbProductName.Size = new System.Drawing.Size(158, 21);
            this.cmbProductName.TabIndex = 23;
            this.cmbProductName.SelectedIndexChanged += new System.EventHandler(this.cmbProductName_SelectedIndexChanged);
            // 
            // txt_productName
            // 
            this.txt_productName.Location = new System.Drawing.Point(361, 92);
            this.txt_productName.Name = "txt_productName";
            this.txt_productName.Size = new System.Drawing.Size(158, 20);
            this.txt_productName.TabIndex = 24;
            // 
            // lbl_error
            // 
            this.lbl_error.AutoSize = true;
            this.lbl_error.Location = new System.Drawing.Point(519, 347);
            this.lbl_error.Name = "lbl_error";
            this.lbl_error.Size = new System.Drawing.Size(0, 13);
            this.lbl_error.TabIndex = 25;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(265, 229);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(50, 13);
            this.label1.TabIndex = 26;
            this.label1.Text = "Quantite:";
            // 
            // num_quantite
            // 
            this.num_quantite.Location = new System.Drawing.Point(361, 222);
            this.num_quantite.Maximum = new decimal(new int[] {
            99999,
            0,
            0,
            0});
            this.num_quantite.Name = "num_quantite";
            this.num_quantite.Size = new System.Drawing.Size(158, 20);
            this.num_quantite.TabIndex = 27;
            this.num_quantite.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // AdminForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(771, 450);
            this.Controls.Add(this.num_quantite);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.lbl_error);
            this.Controls.Add(this.txt_productName);
            this.Controls.Add(this.cmbProductName);
            this.Controls.Add(this.btn_save);
            this.Controls.Add(this.btn_delete);
            this.Controls.Add(this.btn_edit);
            this.Controls.Add(this.btn_add);
            this.Controls.Add(this.num_productCab);
            this.Controls.Add(this.btn_cancel);
            this.Controls.Add(this.txt_productPrice);
            this.Controls.Add(this.lbl_cab);
            this.Controls.Add(this.lbl_productPrice);
            this.Controls.Add(this.lbl_productName);
            this.Controls.Add(this.btn_selectImg);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.lbl_addNewProduct);
            this.Name = "AdminForm";
            this.Text = "AdminForm";
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.num_productCab)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.productBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.productBindingSource1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.num_quantite)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lbl_addNewProduct;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Button btn_selectImg;
        private System.Windows.Forms.Label lbl_productName;
        private System.Windows.Forms.Label lbl_productPrice;
        private System.Windows.Forms.Label lbl_cab;
        private System.Windows.Forms.TextBox txt_productPrice;
        private System.Windows.Forms.Button btn_cancel;
        private System.Windows.Forms.NumericUpDown num_productCab;
        private System.Windows.Forms.Button btn_add;
        private System.Windows.Forms.Button btn_edit;
        private System.Windows.Forms.Button btn_delete;
        private System.Windows.Forms.Button btn_save;
        //private CashSystemDataSet1 cashSystemDataSet1;
        private System.Windows.Forms.BindingSource productBindingSource;
        //private CashSystemDataSet1TableAdapters.ProductTableAdapter productTableAdapter;
        private System.Windows.Forms.BindingSource productBindingSource1;
        private System.Windows.Forms.ComboBox cmbProductName;
        private System.Windows.Forms.TextBox txt_productName;
        private System.Windows.Forms.Label lbl_error;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.NumericUpDown num_quantite;
    }
}