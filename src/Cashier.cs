﻿using System;
using System.Collections.Generic;
using System.Linq;


namespace CashSys {
    class Cashier : Person {
        private string username, password;

        static uint count = 0;
        public Cashier() {
            //initTickets("Cashier");
            Id = ++count;
        }

        public uint Id { get; }
        public string Username { get; set; }
        public string Password { get; }
        public void setPassword() {
            string p = Console.ReadLine();
            while (p.Length < 6) {
                Console.WriteLine("Please provide a password that have at least 6 chars: ");
                p = Console.ReadLine();
            }
            password = p;
        }
        public bool setPassword(string p) {
            if (p.Length >= 6) {
                password = p;
                return true;
            }
            else {
                Console.WriteLine("Please provide a password that have at least 6 chars.");
                return false;
            }
        }
       
        public bool updatePassword(string newPassword) {
            if (AuthDB.updatePassword(username, password))
                return true;

            return false;
        }

        public bool auth(Machine m) {
            if(m.isConnected()) {
                Console.WriteLine($"Cannot connect to the machine. ID: {m.getId()}");
                Console.WriteLine("Machine already user by another cashier.");
                return false;
            }
            else {
                if(AuthDB.login(username, password)) {
                    m.setConnection(username);
                    Console.WriteLine($"Successfully connected to machine {m.getId()}.");

                    return true;
                }
                return false;
            }

        }

        public void disconnect(Machine m) {
            m.endConnection(username);
        }
    }
}
