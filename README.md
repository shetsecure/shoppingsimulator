# ShoppingSimulator

C# project that simulates buying and managing products in a store.
This project have 2 main GUI interfaces.

<h1>Features</h1>
<ul>
    <li>
        <h3>Admin Interface</h3>
        <ul>
            <li>Logging in</li>
            <li>Managing products</li>
            <li>Managing cash machine</li>
        </ul>
    </li>
    <li>
        <h3>Customer Interface</h3>
        <ul>
            <li>Buying products</li>
            <li>Get the bill</li>
        </ul>
    </li>
</ul>

<h1>ScreenShots</h1>

<img src="screens/1.PNG" />
<h2>Admin Interface </h2>
<img src="screens/admin1.PNG" />
<img src="screens/admin2.PNG" />
<img src="screens/admin3.PNG" />
<img src="screens/admin4.PNG" />
<img src="screens/admin5.PNG" />

<h2> Customer Interface </h2>
<img src="screens/cust1.PNG" />
<img src="screens/cust2.PNG" />
<img src="screens/cust3.PNG" />